#!/usr/bin/env bash

# OSlist: https://git-scm.com/download/linux
# Linux
export OS_DEBIAN="Debian"
export OS_UBUNTU="Ubuntu"
export OS_REDHAT="RedHat" # Red Hat Enterprise Linux, Oracle Linux, CentOS, Scientific Linux, et al.
export OS_FEDORA="Fedora"
export OS_ARCH="ArchLinux"
export OS_GENTOO="Gentoo"
export OS_SUSE="openSUSE"
export OS_SLACKWARE="Slackware"
export OS_NIXOS="NixOS"
export OS_ALPINE="Alpine" # musl libc| busybox https://www.linuxprobe.com/alpine-linux-released.html
export OS_VOID="VoidLinux"
export OS_MAGEIA="Mageia" # Mandriva Linux
export OS_SLITAZ="Slitaz" # busybox
export OS_GUIXSD="GuixSD"
# Unix
export OS_MACOS="macOS"
export OS_AIX="AIX"
export OS_SOLARIS="Solaris" #OpenCSW illumos(kernel):OpenIndiana SmartOS DogeOS
export OS_FREEBSD="FreeBSD"
export OS_OPENBSD="OpenBSD"
export OS_NETBSD="NetBSD"
export OS_DFBSD="DragonflyBSD"
export OS_TRUEOS="TrueOS"
# Windows
export OS_CYGWIN="Cygwin"
export OS_MSYS2="Msys2"
# Other
export OS_UNKNOW="Unknow"

# PKG-list: Yum APT SUSE AUR

# Arch-list:
#https://github.com/llvm-mirror/llvm/blob/a1cdcbb384fcf1a0332b3d8b7a9240e5c3ca5a14/docs/GettingStarted.rst#local-llvm-configuration
# AArch64, AMDGPU, ARM, BPF, CppBackend, Hexagon, Mips, MSP430, NVPTX, PowerPC, Sparc, SystemZ X86, XCore

export OME_ARCH="`uname -m`"
# export OME_SHELL="`ps | grep $$ | awk '{print $4}'`"
export OME_SHELL="`basename $SHELL`"

case "`uname -o`" in
    "GNU/Linux")
        if [ "`command -v dnf`" != "" ]; then
            export OME_OS=$OS_FEDORA
        elif [ "`command -v yum`" != "" ]; then
            export OME_OS=$OS_REDHAT
        elif [ "`command -v zypper`" != "" ]; then
            export OME_OS=$OS_SUSE
        elif [ "`command -v nix`" != "" ]; then
            export OME_OS=$OS_NIXOS
        elif [ "`command -v guix`" != "" ]; then
            export OME_OS=$OS_GUIXSD
        elif [ "lsb_release" != "" ]; then
            case "`lsb_release -is`" in
                "Debian")
                    export OME_OS=$OS_DEBIAN;;
                "Ubuntu"|"elementary"|"LinuxMint")
                    export OME_OS="$OS_UBUNTU";;
                "ManjaroLinux"|"Arch")
                    export OME_OS=$OS_ARCH;;
                *)
                    export OME_OS="`lsb_release -is`";;
            esac
        else
            export OME_OS=$OS_UNKNOW
        fi
        ;;
    "Cygwin") export OME_OS=$OS_CYGWIN;;
    "Msys") export OME_OS=$OS_MSYS2;;
    "FreeBSD") export OME_OS=$OS_FREEBSD;;
    "Darwin") export OME_OS=$OS_MACOS;;
    *) export OME_OS=$OS_UNKNOW;;
esac

################################################################################

# Oh-My-Emacs
# export OME_BIN=$(dirname $(readlink -f $0))
# export OME_ROOT=$OME_BIN/..
export OME_ROOT=$HOME/.oh-my-env
export OME_BIN=$OME_ROOT/bin

export OME_PREFIX=$HOME/.ome_local
export OME_REPO=$HOME/.ome_local/repo
export OME_WS=$HOME/workspace
export OME_BOOK=$HOME/book
export OME_PROJ=$HOME/projects

export PATH=$OME_WS/bin:$OME_BIN:$PATH

# Emacs
export EDITOR=em
#export ALTERNATE_EDITOR=""
export OME_EMACS=$HOME/.oh-my-emacs
export OME_EMACS_MODULE=$HOME/.oh-my-emacs/emacs-config/modules
export OME_EMACS_SNIPPET=$HOME/.oh-my-emacs/emacs-config/snippets

# lib
. $HOME/.oh-my-env/bin/lib
. $HOME/.oh-my-env/bin/tool
. $HOME/.oh-my-env/bin/proj

################################################################################
export OME_SHOW=""
ome_show() {
    local index=0
    for item in $@; do
        if [ "$index" = "0" ]; then
            export OME_SHOW="${OME_SHOW}${item}\t"
        else
            export OME_SHOW="$OME_SHOW${item} "
        fi
        let index++
    done
    export OME_SHOW="$OME_SHOW\n"
}

# echo_kv "OME" "OS:$OME_OS | Arch:$OME_ARCH | Shell:$OME_SHELL | PREFIX:$OME_PREFIX"
# echo_kv "VCS" "`git --version | head -n 1` | `svn --version | head -n 1`"
# echo_kv "Perl" "`perl -V | head -n 1 | cut -c 15-57`"
ome_show "SYSTEM" "OS:$OME_OS | Arch:$OME_ARCH | Shell:$OME_SHELL | EDITOR:$EDITOR"
ome_show "PREFIX" "\"$OME_PREFIX\""
ome_show "VCS" "`ome_fetch_version git` | `ome_fetch_version svn`"

################################################################################
# load plugins
for plugin in $HOME/.oh-my-env/plugins/* ; do
    if [ "$plugin" != "$HOME/.oh-my-env/plugins/README.org" ]; then
        # echo "name: $plugin"
        . "$plugin"
    fi
done
################################################################################
# User Settings
# if [ -d /mingw64/bin2 ]; then
# export PATH=/mingw64/bin:$PATH
# export MANPATH=/mingw64/share/man:$MANPATH
# export INFOPATH=/mingw64/share/info:$INFOPATH
# export C_INCLUDE_PATH=/mingw64/include:$C_INCLUDE_PATH
# export CPLUS_INCLUDE_PATH=/mingw64/include:$CPLUS_INCLUDE_PATH
# export LIBRARY_PATH=/mingw64/lib:$LIBRARY_PATH
# export LD_LIBRARY_PATH=/mingw64/lib:$LD_LIBRARY_PATH
# export PKG_CONFIG_PATH=/mingw64/lib/pkgconfig:/mingw64/share/pkgconfig:$PKG_CONFIG_PATH
# fi
ome_dev_path $OME_PREFIX
ome_dev_path $HOME/.local

# alias
alias ls='ls --color'
alias wget='wget -c'
alias omecd='f(){ cd $OME_ROOT/$1; ls; unset -f f; }; f'
alias pjcd='f(){ cd $OME_PROJ/$1; ls; unset -f f; }; f'
alias emcd='f(){ cd $OME_EMACS/$1; ls; unset -f f; }; f'
alias repocd='f(){ cd $OME_REPO/$1; ls; unset -f f; }; f'
# alias opam='opam --debug'

if [ "$OME_OS" = "$OS_CYGWIN" ] || [ "$OME_OS" = "$OS_MSYS2" ]; then
    export LANG=en_US.UTF-8
fi

if [ "$OME_OS" = "$OS_DEBIAN" ] || [ "$OME_OS" = "$OS_UBUNTU" ]; then
    alias apt-dist-upgrade='f(){ sudo apt update && sudo apt dist-upgrade; unset -f f; }; f'
fi

# bugfix:emacs crash
export XLIB_SKIP_ARGB_VISUALS=1
